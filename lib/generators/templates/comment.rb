class Comment < ActiveRecord::Base
  include ActsAsComments::CommentModule

  acts_as_votable
  attr_accessible :title,
                  :content,
                  :is_published,
                  :commentable_id,
                  :commentable_type,
                  :id_deleted,
                  :created_at,
                  :updated_at,
                  :user_id,
                  :parent_id

  # Relations
  # ================================================================
  validates_format_of :content, without: /\.(at|b(y|e|iz)|c(a|c|h|om|n)|d(e|k)|e(du|e|s|u)|i(l|n|o|s|t)|hu|fr|md|n(ame|et|l|o)|org|pl|([-\d\*$&\%_\s]{0,3}r[-\d\*$&\%_\s]{1,3}u|[-\d\*$&\%_\s]{1,3}ru|ru)|s(e|k|t|u)|t(k|o|v)|ws|u(a|k|s))/i, message: "Не допускается размещение ссылок"
  validates :content, presence: true

  def self.get_comments(params)
    comments = self.where(commentable_id: params[:id], commentable_type: params[:type]).order(:lft)
    comments.includes(user: [:main_image, :votes]) if self.respond_to?(:user)
    comments
  end

  def user_name(field)
    if self.respond_to?(:user) && !self.user_id.nil?
      User.find(self.user_id).send(field.to_sym)
    else
      I18n.t('user.anonim')
    end
  end
end