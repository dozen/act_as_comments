class CreateComments < ActiveRecord::Migration
  def change
    create_table :comments, force: true, comment: 'Комментарии frontend-пользователей' do |t|
      t.text :content, comment: 'Содержимое комментария'
      t.integer :commentable_id, null: false, comment: 'Внешний ключ для полиморфной связи с комментируемым объектом'
      t.string  :commentable_type, null: false, comment: 'Внешний ключ для полиморфной связи с комментируемым объектом'
      t.integer :user_id, comment: 'Внешний ключ для связи с таблицей пользователей'
      t.boolean :is_published, default: true, comment: '1 - комментарий опубликован, 0 - комментарий скрыт от frontend-пользователей'
      t.boolean :is_deleted, default: false, comment: '1 - комментарий удален, 0 - комментарий доступен для backend- и frontend-пользователей'
      t.integer :lft, comment: 'Левая граница в рамках вложенного множества Nested Set'
      t.integer :rgt, comment: 'Правая граница в рамках вложенного множества Nested Set'
      t.integer :parent_id, comment: 'Внешний ключ для связи с родительским комментарием'
      t.integer :depth, comment: 'Глубина комментария в рамках вложенного множества Nested Set'
      t.integer :cached_weighted_score, default: 0, comment: 'Кэш для лайков'
      t.integer :children_count, null: false, default: 0, comment: 'Количество вложенных комментариев'
      t.timestamps
    end

    add_index  :comments, :cached_weighted_score
    add_index :comments, [:user_id]
    add_index :comments, [:commentable_id, :commentable_type]
  end
end