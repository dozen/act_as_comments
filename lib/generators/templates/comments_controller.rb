class CommentsController < ApplicationController
  before_filter :set_commentable_model, only: :create

  def index
    @comments = Comment.get_comments(params)
    respond_to do |format|
      format.html { @comments }
      format.json { render json: as_json(comments), root: false }
    end
  end

  # POST /comments
  def create
    user = self.respond_to?(:current_user) ? current_user : nil
    parent = params['parent_id'].present? ? params['parent_id'] : nil
    @comment = Comment.build_from(@commentable, params[:content], user.try(:id), parent)

    url = "/#{params[:commentable_type].downcase.pluralize}/#{params[:commentable_id]}"

    if @comment.save
      if Setting && Setting.moderate_comment == "1"
        respond_to do |format|
          msg = t('msg.moderate_comment')
          format.html { redirect_to url, notice: msg }
          format.json { render json: {notice: msg} }
        end
      else
        respond_to do |format|
          msg =  t('msg.saved')
          format.html { redirect_to url, notice: msg }
          format.json { render json: {notice: msg, comment: single_as_json(@comment)}, root: false }
        end
      end
    else
      respond_to do |format|
        msg = @comment.errors.full_messages
        format.html { redirect_to url, notice: msg }
        format.json { render json: {errors: msg } }
      end
    end
  end

  def like
    if self.respond_to?(:current_user)
      comm = Comments::Comment.find(params[:id])
      url = "/#{comm.commentable.class.name.downcase.pluralize}/#{comm.commentable.id}"

      if User.last.voted_up_on? comm
        notice = t('msg.already_voted')
      else
        comm.liked_by User.last
        ActiveRecord::Base.connection.execute("update users SET cache_count_votes = (select SUM(cached_weighted_score) as votes from comments where user_id = #{comm.user_id}) where users.id = #{comm.user_id}") if comm.user_id.present?
        notice = t('msg.voted')
      end

      respond_to do |format|
        format.json { render json: {notice: notice, score: comm.cached_weighted_score } }
        format.html { redirect_to url, notice: notice }
      end
    else
      nil
    end
  end

  def dislike
    if self.respond_to?(:current_user)
      comm = Comment.find(params[:id])
      url = "/#{comm.commentable.class.name.downcase.pluralize}/#{comm.commentable.id}"

      if User.last.voted_down_on? comm
        notice = t('msg.already_voted')
      else
        comm.disliked_by User.last
        ActiveRecord::Base.connection.execute("update users SET cache_count_votes = (select SUM(cached_weighted_score) as votes from comments where user_id = #{comm.user_id}) where users.id = #{comm.user_id}") if comm.user_id.present?
        notice = t('msg.voted')
      end

      respond_to do |format|
        format.html { redirect_to url, notice: notice }
        format.json { render json: {notice: notice, score: comm.cached_weighted_score } }
      end
    else
      nil
    end
  end

  private
  def set_commentable_model
    class_name = params[:commentable_type].to_s.classify.constantize
    @commentable = class_name.find_by_id(params[:commentable_id]) || Rack::Response.new(["Not Found"], 404).finish
  end

  # def as_json(comments)
  #   liked = if current_user then current_user.likes_id(comments) else [] end
  #   comments.map{|comment| to_json(comment) }
  # end
  #
  # def single_as_json(comment)
  #   liked = if current_user then current_user.likes_id(comment) else [] end
  #   to_json(comment)
  # end
  #
  # def to_json(comment)
  #   {
  #     id: comment.id,
  #     content: comment.content,
  #     parent_id: comment.parent_id,
  #     user_image: user_image(comment),
  #     likes: comment.cached_weighted_score,
  #     user_comment: user_comment?(comment),
  #     username: comment.try(:user).try(:full_name) || 'Аноним',
  #     depth: comment.depth,
  #     added_time: view_context.time_ago_in_words(comment.created_at)
  #   }
  # end
  #
  # def user_image(comment)
  #   comment && comment.user ? comment.user.main_image.url('_60x60') : ''
  # end
  #
  # def user_comment?(comment)
  #   current_user && comment.user && current_user.id === comment.user.id ? true : false
  # end
end