require 'rails/generators/migration'

class CommentGenerator < Rails::Generators::Base
  include Rails::Generators::Migration

  def self.source_root
    @_acts_as_comments_source_root ||= File.expand_path("../templates", __FILE__)
  end

  def self.next_migration_number(path)
    Time.now.utc.strftime("%Y%m%d%H%M%S")
  end

  def create_model_file
    copy_file 'comment.rb', 'app/models/comment.rb'
    copy_file 'comments_controller.rb', 'app/controllers/comments_controller.rb'
    migration_template 'create_comments.rb', 'db/migrate/create_comments.rb'

    inject_into_file 'config/routes.rb', after: "#{Rails.application.class.parent_name}::Application.routes.draw do\n" do <<-'RUBY'

  #-------------- Comments Routes

  scope 'comments' do
    get  '/',                           to: 'comments#index'
    post '/',                           to: 'comments#create'
    put  '/like/:id',                   to: 'comments#like'
    put  '/dislike/:id',                to: 'comments#dislike'
    get  '/:id/update_vote/:type',      to: 'comments#update_vote'
  end

  #--------------

    RUBY

    end

    puts '  ------------------------'
    puts '   if you want votes for your comments please make this steps'
    puts '   rails generate acts_as_votable:migration'
    puts '   rake db:migrate'
    puts '  ------------------------'
  end
end