module ActsAsComments
  module CommentModule
    extend ActiveSupport::Concern

    included do
      belongs_to     :commentable, polymorphic: true
      acts_as_nested_set scope: [:commentable_id, :commentable_type], dependent: :destroy
      after_create   :update_user_counter_cache
      after_create   :update_commentable_counter_cache
      after_create   :update_children_count
      after_destroy  :update_commentable_counter_cache_decrement
      after_destroy  :update_user_counter_cache_decrement
      after_destroy  :update_children_count_decrement

      def self.build_from(obj, comment, user_id, parent)
        new do |c|
          c.commentable  = obj
          c.content      = comment
          c.user_id      = user_id
          c.parent_id    = parent
          c.is_published = modarate? ? false : true
        end
      end

      def self.destroy_all(conditions = {})
        where(conditions.merge(parent_id: nil)).destroy_all
      end

      def self.modarate_comments param
        @modarate = param
      end

      def self.modarate?
        @modarate = false if @modarate.nil?
        @modarate.respond_to?(:call) ? @modarate.call : @modarate
      end
    end

    def update_children_count
      if self.class.column_names.include?('children_count')
        self.increment!(:children_count)
      end
    end

    def update_children_count_decrement
      if self.class.column_names.include?('children_count')
        self.decrement!(:children_count)
      end
    end

    def update_commentable_counter_cache
      if Object.const_get(self.commentable_type).column_names.include?('comments_count')
        commentable.increment!(:comments_count)
      end
    end

    def update_commentable_counter_cache_decrement
      if self.commentable && Object.const_get(self.commentable_type).column_names.include?('comments_count')
        commentable.decrement!(:comments_count)
      end
    end

    def update_user_counter_cache
      if !self.user_id.nil? && User.column_names.include?('comments_count')
        ActiveRecord::Base.connection.execute("UPDATE `users` SET `comments_count` = COALESCE(`comments_count`, 0) + 1 WHERE `users`.`id` = #{self.user_id}")
      end
    end

    def update_user_counter_cache_decrement
      if !self.user_id.nil? && User.column_names.include?('comments_count')
        ActiveRecord::Base.connection.execute("UPDATE `users` SET `comments_count` = COALESCE(`comments_count`, 0) - 1 WHERE `users`.`id` = #{self.user_id}")
      end
    end

    def move_to_parent(parent_id)
      move_to_child_of(Comment.find(parent_id))
    end

  end
end