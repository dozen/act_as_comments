module ActsAsComments
  module Comments

    def acts_as_comments
      has_many :comments, as: :commentable, class_name: 'Comment'
      after_destroy do
        Comment.destroy_all(commentable_id: self.id)
      end
    end
  end
end