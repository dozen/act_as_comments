require "acts_as_comments/version"
require 'active_support'

module ActsAsComments
  require 'awesome_nested_set'
  require 'acts_as_votable'
  require 'acts_as_comments/comment_module'
  if defined?(ActiveRecord::Base)
    require 'acts_as_comments/acts_as_comments'
    ActiveRecord::Base.extend ActsAsComments::Comments
  end
end
