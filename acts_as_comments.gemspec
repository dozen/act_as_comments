# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'acts_as_comments/version'

Gem::Specification.new do |spec|
  spec.name          = "acts_as_comments"
  spec.version       = ActsAsComments::VERSION
  spec.authors       = ["SergeyMild"]
  spec.email         = ["sergeymild@yandex.ru"]
  spec.summary       = %q{Comments for rails applications.}
  spec.description   = %q{Comments for rails applications.}
  spec.homepage      = "https://github.com/sergeymild"
  spec.license       = "MIT"

  spec.files         = `git ls-files -z`.split("\x0")
  spec.executables   = spec.files.grep(%r{^bin/}) { |f| File.basename(f) }
  spec.test_files    = spec.files.grep(%r{^(test|spec|features)/})
  spec.require_paths = ["lib"]

  spec.add_dependency 'awesome_nested_set'
  spec.add_dependency 'acts_as_votable', '~> 0.10.0'
  spec.add_dependency 'activesupport'

  spec.add_development_dependency "bundler", "~> 1.6"
  spec.add_development_dependency 'sqlite3'
  spec.add_development_dependency "rake"
end
